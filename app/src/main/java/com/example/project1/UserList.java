package com.example.project1;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import com.example.project1.adapter.UserListAdapter;
import com.example.project1.util.Const;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;

public class UserList extends AppCompatActivity {
    ListView lvUsers;
    //Spinner spUsers;
    ArrayList<HashMap<String,Object>> userList = new ArrayList<>();
    UserListAdapter userListAdapter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.user_list);
        lvUsers = findViewById(R.id.lvActUserList);
       // spUsers = findViewById(R.id.spActUsers);
        userList.addAll((Collection<? extends HashMap<String, Object>>)getIntent().getSerializableExtra("UserList"));
        userListAdapter = new UserListAdapter(this, userList);
        lvUsers.setAdapter(userListAdapter);
       // spUsers.setAdapter(userListAdapter);

        lvUsers.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long iteamId) {
                Toast.makeText(UserList.this, userList.get(position).get(Const.GENDER).toString(), Toast.LENGTH_SHORT).show();
            }
        });

    }
}
