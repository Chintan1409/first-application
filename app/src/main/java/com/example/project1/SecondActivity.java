package com.example.project1;

import android.content.Intent;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

public class SecondActivity extends AppCompatActivity {
    TextView tvDisplay,Phone,Email;
    ImageView ivDisplay;
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);

        tvDisplay = findViewById(R.id.tvActDisplay);
        ivDisplay = findViewById(R.id.ivActDisplay);
        Phone = findViewById(R.id.tvActDisplayPhone);
        Email = findViewById(R.id.tvActDisplayEmail);
        String value = getIntent().getStringExtra("str");
        String phone = getIntent().getStringExtra("phone");
        String email = getIntent().getStringExtra("email");
        tvDisplay.setText(value);
        Phone.setText(phone);
        Email.setText(email);
        ivDisplay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }

}

