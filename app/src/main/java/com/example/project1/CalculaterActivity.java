package com.example.project1;

import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
public class CalculaterActivity extends AppCompatActivity {
    TextView tvDisplay;
    ImageView ivDisplay;
    Button btnCE,btnPer,btnPM,btnDiv,btn7,btn8,btn9,btnMul,btn4,btn5,btn6,btnSub,btnAdd,btn1,btn2,btn3,btnDel,btn0,btnDot,btnAns ;
    TextView TVDisplay;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.callculater);
        initViewReference();
        allButton();
    }
    void  allButton() {
        btn1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                TVDisplay.setText("1");
            }
        });
        btn2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                TVDisplay.setText("2");
            }
        });
        btn3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                TVDisplay.setText("3");
            }
        });
        btn4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                TVDisplay.setText("4");
            }
        });
        btn5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                TVDisplay.setText("5");
            }
        });
        btn6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                TVDisplay.setText("6");
            }
        });
        btn7.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                TVDisplay.setText("7");
            }
        });
        btn8.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                TVDisplay.setText("8");
            }
        });
        btn9.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                TVDisplay.setText("9");
            }
        });
        btn0.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                TVDisplay.setText("0");
            }
        });
        ivDisplay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }


    void initViewReference() {
        btnCE = findViewById(R.id.btnActCE);
        btnPer = findViewById(R.id.btnActPer);
        btnPM = findViewById(R.id.btnActPM);
        btnDiv = findViewById(R.id.btnActDiv);
        btnMul = findViewById(R.id.btnActMul);
        btnSub = findViewById(R.id.btnActSub);
        btnAdd= findViewById(R.id.btnActAdd);
        btnDel= findViewById(R.id.btnActDel);
        btnAns= findViewById(R.id.btnActAns);
        btnDot= findViewById(R.id.btnActDot);
        btn1 = findViewById(R.id.btnAct1);
        btn2 = findViewById(R.id.btnAct2);
        btn3 = findViewById(R.id.btnAct3);
        btn4 = findViewById(R.id.btnAct4);
        btn5 = findViewById(R.id.btnAct5);
        btn6 = findViewById(R.id.btnAct6);
        btn7 = findViewById(R.id.btnAct7);
        btn8 = findViewById(R.id.btnAct8);
        btn9 = findViewById(R.id.btnAct9);
        btn0 = findViewById(R.id.btnAct0);
        TVDisplay = findViewById(R.id.tvActDisplay);
        ivDisplay = findViewById(R.id.ivActDisplay);
    }
}

