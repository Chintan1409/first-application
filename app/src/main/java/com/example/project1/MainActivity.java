package com.example.project1;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import android.content.Intent;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.example.project1.database.MyDatabase;
import com.example.project1.database.TblUserData;
import com.example.project1.util.Const;
import java.util.ArrayList;
import java.util.HashMap;

public class MainActivity extends AppCompatActivity {
    EditText etFirstName,etEmail,etPhoneNumber;
    Button btnSubmit,btnGo,btnCal,btnList,btnPhoneNumber,btnName,btnEmail;
    ImageView ivDisply,ivSerch;
    RadioButton rbMale,rbFemale;
    CheckBox chbCricket,chbHockey,chbFootBall;
    RadioGroup rgGender;
    private long backPressedTime;

    ArrayList<HashMap<String,Object>> userList =new ArrayList<>();

    @Override
    public void onBackPressed() {
        if (backPressedTime + 3000 > System.currentTimeMillis()) {
            super.onBackPressed();
            return;
        } else {
            Toast.makeText(getBaseContext(), "Press Back Again To Exit", Toast.LENGTH_SHORT).show();
        }
        backPressedTime = System.currentTimeMillis();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initViewReference();
        setTypefaceOnView();
        bindViewValues();

        ivDisply.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        if (rbFemale.isChecked()) {
            chbHockey.setVisibility(View.GONE);
        }

        rgGender.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {
                if(i == R.id.rbActMale){
                    chbCricket.setVisibility(View.VISIBLE);
                    chbHockey.setVisibility(View.VISIBLE);
                    chbFootBall.setVisibility(View.VISIBLE);
                }
                else{
                    chbCricket.setVisibility(View.VISIBLE);
                    chbHockey.setVisibility(View.GONE);
                    chbFootBall.setVisibility(View.VISIBLE);
                }
            }
        });
    }
    void bindViewValues() {
        ivSerch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.google.com"));
                startActivity(intent);
            }
        });

        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (isValid()) {
                String firstName = etFirstName.getText().toString();
                //String lastName = etLastName.getText().toString();
                String fNameUpper = firstName.substring(0, 1).toUpperCase() + firstName.substring(1).toLowerCase();
                //String lNameUpper = lastName.substring(0, 1).toUpperCase() + lastName.substring(1).toLowerCase();
                //String temp = fNameUpper +"  "+ lNameUpper;
                Toast.makeText(getApplicationContext(), fNameUpper ,Toast.LENGTH_LONG).show();
            }}
        });

        btnGo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (isValid()) {
                Intent intent = new Intent(MainActivity.this, SecondActivity.class);
                String firstName = etFirstName.getText().toString();

                String email = etEmail.getText().toString();
                String phone = etPhoneNumber.getText().toString();
                String fNameUpper = firstName.substring(0, 1).toUpperCase() + firstName.substring(1).toLowerCase();
                // String lNameUpper = lastName.substring(0, 1).toUpperCase() + lastName.substring(1).toLowerCase();
                // String temp = fNameUpper +"  "+ lNameUpper;
                String str = fNameUpper.toString();
                intent.putExtra("str",str);
                intent.putExtra("phone",phone);
                intent.putExtra("email",email);
                startActivity(intent);
            }}
        });
        btnCal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, CalculaterActivity.class);
                startActivity(intent);
            }
        });
        btnList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (isValid()) {
                    etFirstName.requestFocus();
                    etPhoneNumber.requestFocus();
                    etEmail.requestFocus();
                    HashMap<String, Object> map = new HashMap<>();
                    map.put(Const.FIRST_NAME, etFirstName.getText().toString());
                    map.put(Const.EMAIL_ADDRESS, etEmail.getText().toString());
                    //map.put(Const.PHONE_NUMBER, etPhoneNumber.getText().toString());
                    map.put(Const.GENDER, rbMale.isChecked() ? rbMale.getText().toString() : rbFemale.getText().toString());
                    String hobby = "";
                    if (chbCricket.isChecked()) {
                        hobby += chbCricket.getText().toString();
                    }
                    if (chbFootBall.isChecked()) {
                        hobby += "," + chbFootBall.getText().toString();
                    }
                    if (chbHockey.isChecked()) {
                        hobby += "," + chbHockey.getText().toString();
                    }
                    map.put(Const.HOBBY, hobby);
                    userList.add(map);
                    Intent intent = new Intent(MainActivity.this, UserList.class);
                    intent.putExtra("UserList", userList);
                    startActivity(intent);

                    etFirstName.setText("");
                    etEmail.setText("");
                    etPhoneNumber.setText("");

                    String name = etFirstName.getText().toString();
                    String phonenumber = etPhoneNumber.getText().toString();
                    String email = etEmail.getText().toString();
                    String gender = rbMale.isChecked() ? rbMale.getText().toString() : rbFemale.getText().toString();
                    TblUserData tblUserData = new TblUserData(MainActivity.this);
                    long lastInsrtedId=tblUserData.insertUserDetail(name,phonenumber,email,gender,hobby);
                    Toast.makeText(getApplicationContext(), lastInsrtedId>0 ?"User Inserted Successfully":"User Inserted Successfully",Toast.LENGTH_SHORT).show();
                }
            }
        });

        btnName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                etFirstName.setText("");
            }
        });

        btnPhoneNumber.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                etPhoneNumber.setText("");
            }
        });

        btnEmail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                etEmail.setText("");
            }
        });

    }
    void setTypefaceOnView() {
        Typeface type = Typeface.createFromAsset(getAssets(),"fonts/NotoSerif-Bold.ttf");
        Typeface type1 = Typeface.createFromAsset(getAssets(),"fonts/NotoSerif-BoldItalic.ttf");
        Typeface type2 = Typeface.createFromAsset(getAssets(),"fonts/NotoSerif-Italic.ttf");
        etFirstName.setTypeface(type1);
        etEmail.setTypeface(type1);
        etPhoneNumber.setTypeface(type1);
        btnSubmit.setTypeface(type2);
        btnGo.setTypeface(type2);
        btnCal.setTypeface(type2);
        rbMale.setTypeface(type1);
        rbFemale.setTypeface(type1);
        chbCricket.setTypeface(type2);
        chbHockey.setTypeface(type2);
        chbFootBall.setTypeface(type2);
    }
    void initViewReference() {
        new MyDatabase(MainActivity.this).getReadableDatabase();

        etFirstName=findViewById(R.id.etActFirstName);
        btnSubmit = findViewById(R.id.btnActSubmit);
        btnGo = findViewById(R.id.btnActGo);
        ivDisply = findViewById(R.id.ivActDisplay);
        ivSerch = findViewById(R.id.ivActSerch);
        btnCal = findViewById(R.id.btnActCal);
        etPhoneNumber = findViewById(R.id.etActPhoneNumber);
        etEmail = findViewById(R.id.etActEmail);
        rbMale = findViewById(R.id.rbActMale);
        rbFemale = findViewById(R.id.rbActFemale);
        chbCricket = findViewById(R.id.chbActCricket);
        chbFootBall = findViewById(R.id.chbActFootBall);
        chbHockey = findViewById(R.id.chbActHockey);
        rgGender = findViewById(R.id.rgActGender);
        btnList = findViewById(R.id.btnActList);
        btnName = findViewById(R.id.btnActName);
        btnPhoneNumber = findViewById((R.id.btnActPhone));
        btnEmail = findViewById(R.id.btnActEmail);
        getSupportActionBar().setTitle(R.string.lbl_main_activity);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if(item.getItemId() == R.id.menu_about){
            Toast.makeText(MainActivity.this, "Menu Button Pressed",Toast.LENGTH_SHORT).show();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu, menu);
        return super.onCreateOptionsMenu(menu);

    }

    boolean isValid() {
        boolean flag = true;
        if (TextUtils.isEmpty(etEmail.getText())) {
            etEmail.setError(getString(R.string.error_enter_value));
            flag = false;
            etEmail.requestFocus();
        } else {
            String email = etEmail.getText().toString();
            String emailPattern = "([a-zA-Z0-9._-]+)@([a-z]+)\\.([a-z]{2,8})(\\.[a-z]{2,8})?";
            if (!email.matches(emailPattern)) {
                etEmail.setError("Enter Valid Email Address");
                flag = false;
                etEmail.requestFocus();
            }
        }
        if (TextUtils.isEmpty(etPhoneNumber.getText())) {
            etPhoneNumber.setError(getString(R.string.error_enter_value));
            flag = false;
            etPhoneNumber.requestFocus();
        } else {
            String phoneNumber = etPhoneNumber.getText().toString();
            if (phoneNumber.length() < 10) {
                etPhoneNumber.setError("Enter Valid Phone Number");
                flag = false;
                etPhoneNumber.requestFocus();
            }
        }
        if (TextUtils.isEmpty(etFirstName.getText().toString().trim())) {
            etFirstName.setError(getString(R.string.error_enter_value));
            flag = false;
            etFirstName.requestFocus();
        } else {
            String name = etFirstName.getText().toString();
            String namePattern = "([a-zA-Z ]+)";
            if (!name.matches(namePattern)) {
                etEmail.setError("Enter Valid Name");
                flag = false;
                etFirstName.requestFocus();
            }
        }
        if (!(chbCricket.isChecked() || chbFootBall.isChecked() || chbHockey.isChecked())) {
            Toast.makeText(this, "Please Select any one Checkbox", Toast.LENGTH_LONG).show();
            flag = false;
        }
        return flag;
    }
}
