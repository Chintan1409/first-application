package com.example.project1.adapter;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import com.example.project1.R;
import com.example.project1.util.Const;
import java.util.ArrayList;
import java.util.HashMap;

import static java.lang.String.valueOf;

public class UserListAdapter extends BaseAdapter {
    Context context;
    ArrayList<HashMap<String, Object>> userList;

    public UserListAdapter(Context context, ArrayList<HashMap<String, Object>> userList) {
        this.context = context;
        this.userList = userList;
    }
    @Override
    public int getCount() {
        return userList.size();
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int position, View view, ViewGroup viewGroup) {
        View view1 = LayoutInflater.from(context).inflate(R.layout.view_row_user_list, null);

        TextView tvName = view1.findViewById(R.id.tvListName);
        TextView tvEmail = view1.findViewById(R.id.tvListEmail);
        TextView tvGender = view1.findViewById(R.id.tvListGender);

        tvName.setText(valueOf(userList.get(position).get(Const.FIRST_NAME)));
        tvEmail.setText(valueOf(userList.get(position).get(Const.EMAIL_ADDRESS)));
        //TextView tvHobby = view1.findViewById(R.id.tvListHobby);
        //tvHobby.setText((valueOf(userList.get(position).get(Const.HOBBY))));

        if (valueOf(userList.get(position).get(Const.GENDER)).contains("Male")) {
            tvGender.setBackgroundResource(R.drawable.male_background);
            tvGender.setText("M");
        }
        else {
            tvGender.setBackgroundResource(R.drawable.female_background);
            tvGender.setText("F");
        }
        return view1;
    }
}
